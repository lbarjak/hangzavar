package eu.barjak.hangzavar;

import com.mongodb.MongoSocketOpenException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.bson.Document;

public class Mongo implements GlobalVariables {

    private MongoCollection<Document> sheetNameCollection;
    private MongoDatabase db;

    Mongo(String url, String dataBase) {
        try {
            MongoClient mongoClient = MongoClients.create(url);
            db = mongoClient.getDatabase(dataBase);
        } catch (MongoSocketOpenException e) {
            e.printStackTrace();
        }
    }

    public void write(String sheetName, LinkedHashMap<String, LinkedHashMap<String, ArrayList>> datas) {

        sheetNameCollection = db.getCollection(sheetName);
        datas.get(sheetName).forEach((k, v) -> {
            //v.remove(0);
            Document row = new Document(k, v);
            sheetNameCollection.insertOne(row);
        });
    }

    public void read(String sheetName, LinkedHashMap<String, LinkedHashMap<String, ArrayList>> datas) {

        sheetNameCollection = db.getCollection(sheetName);

        MongoCursor<Document> cursor = sheetNameCollection.find().projection(Projections.excludeId()).iterator();

        while (cursor.hasNext()) {
            Map document = cursor.next();
            document.forEach((k, v) -> {
                datas.get(sheetName).put((String) k, (ArrayList) v);
            });
        }
        
        datas.get(sheetName).forEach((k, v) -> {
            System.out.println(k + "====>>" + v);
        });
    }

    public void print(String sheetName) {

        sheetNameCollection = db.getCollection(sheetName);
        MongoCursor<Document> cursor = sheetNameCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                System.out.println(cursor.next().toJson());
            }
        } finally {
            cursor.close();
        }
    }
}
/*
        while (cursor.hasNext()) {
            //Document row = (Document) cursor.next(); // Which is already a Map compatible object
            //Map<String, Object> mDoc = row;
            Map document = cursor.next();
            //String key = (String) document.keySet().toArray()[0];
            //datas.get(sheetName).put(key, (ArrayList) document.get(key));
            document.forEach((k, v) -> {
                datas.get(sheetName).put((String) k, (ArrayList) v);
            });
        }

https://jira.mongodb.org/browse/JAVA-2810
Allow dots and $ in field names
https://docs.mongodb.com/manual/reference/limits/#Restrictions-on-Field-Names
https://docs.mongodb.com/manual/core/document/#dot-notation
/var/lib/mongodb
https://stackoverflow.com/questions/6809183/mongo-database-save-data-from-map
https://www.mkyong.com/mongodb/java-mongodb-insert-a-document/
 */
