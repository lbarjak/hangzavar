//R, azaz reduced, mivel a LinkedHashMap miatt nem tartalmaz ismétlődéseket
package eu.barjak.hangzavar;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public interface GlobalVariables {
    
    //HANGZAVAR_MAP-ből leszűrt Elimex termékek
    LinkedHashMap<String, ArrayList> ELIMEX_MAP = new LinkedHashMap<>();
    //Elimex honlapról levett friss adatok
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> ELIMEX_NEW = new LinkedHashMap<>();
    //Elimex honlapról levett előző heti adatok
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> ELIMEX_OLD = new LinkedHashMap<>();
    //Elimex -> Hangtechnika elérhetőségek
    LinkedHashMap<String, String> ELIMEX_HT = new LinkedHashMap<>();
    //Voicekraft -> Hangtechnika elérhetőségek
    LinkedHashMap<String, String> VOICEKRAFT_HT = new LinkedHashMap<>();
    //A hangzavar webáruház teljes mentés XLSX fájl tárolója AY 51 (50) Alapár, CN 92 (91) Gyártó
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> HANGZAVAR_MAP = new LinkedHashMap<>();
    //Sheet1
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> VOICEKRAFT_NEW = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> VOICEKRAFT_OLD = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> ELIMEX_UPLOAD = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> VOICEKRAFT_UPLOAD = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> MONACOR = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> INTERMEDIATE = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> A_FILE = new LinkedHashMap<>();
    
    LinkedHashMap<String, LinkedHashMap<String, ArrayList>> B_FILE = new LinkedHashMap<>();
}
