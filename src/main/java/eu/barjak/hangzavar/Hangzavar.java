package eu.barjak.hangzavar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;

public class Hangzavar implements GlobalVariables {

    public static void main(String[] args) throws IOException, FileNotFoundException, OpenXML4JException {
        Date start = new Date();
        /*
        * Hangzavar
         */
        //readHangzavarXLSX();

        /*
        * HangtechnikaToElimexQuery
         */
        //new HangtechnikaToElimexQuery().query();
        
//CopyToXLSX copytoxlsx = new CopyToXLSX();
//copytoxlsx.write(HANGZAVAR_MAP);
//copytoxlsx.writeout("hangzavar_arlista_map.xlsx");
//
/*
* Monacor
         */
//new MonacorToLinkedHashMap().html2map();
//
/*
* Elimex
         */
//elimexFull();
//elimexTest();
//
/*
* Voice-Kraft
         */
//URL website = new URL("https://voicekraft.shoprenter.hu/custom/voicekraft/image/data/spots/akt.arlista.xlsx");
//String filename = "voicekraft.xlsx";
//new XLSXDownload().download(website, filename);
//            mapInit(VOICEKRAFT_UPLOAD);
//            String newVoicekraftFile = "VK_Arlista_20190210.xlsx";
//            new FromXLSX().read(newVoicekraftFile, VOICEKRAFT_NEW);
//            String oldVoicekraftFile = "VK_Arlista_20190130.xlsx";
//            new FromXLSX().read(oldVoicekraftFile, VOICEKRAFT_OLD);
//            new Voicekraft().print();
//
/*
* Eqaudio
         */
        //new Eqaudio().txtToXLSX();
//
/*
* Database
         */
//            String url = "mongodb+srv://hangzavar:ha8177ma@hangzavar-vwsoh.mongodb.net/test?retryWrites=true";
//            Mongo mongo = new Mongo(url, "hangzavar");
//            mongo.write("export", HANGZAVAR_MAP);
//            mongo.write("columns", HANGZAVAR_MAP);
        //mapInit(INTERMEDIATE);
        //mongo.read("export", INTERMEDIATE);
        //mongo.print("columns");
        
        //emptyUpload();
        
//
/*
* Diff
         */
         new Halmazok().halmazok();
        
        
        Date stop = new Date();
        new Dates().diff(start, stop);
    }

    public static void readHangzavarXLSX() throws FileNotFoundException, OpenXML4JException {

        String xlsxName = "hangtechnika-total-export-5be1282b-1278-2019-02-10_15_47_10.xlsx";
        try {
            new FromXLSX().read(xlsxName, HANGZAVAR_MAP);
        } catch (IOException | InvalidFormatException ex) {
            Logger.getLogger(Hangzavar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void elimexFull() throws IOException, FileNotFoundException, OpenXML4JException {

        mapInit(ELIMEX_UPLOAD);
        ElimexToLinkedHashMap uj = new ElimexToLinkedHashMap();
        uj.html2map();
        //uj.print();//csak teszt
//        String oldElimexFile = "old_elimex20190121_1112.xlsx";
//        new FromXLSX().read(oldElimexFile, ELIMEX_OLD);
//        ElimexHangtechComp elimexHangtechComp = new ElimexHangtechComp();
//        elimexHangtechComp.stockType();
//        elimexHangtechComp.compare();
    }

    public static void elimexTest() throws IOException, FileNotFoundException, OpenXML4JException {

        mapInit(ELIMEX_UPLOAD);
        String oldElimexFile = "old_elimex20190114_1053.xlsx";
        new FromXLSX().read(oldElimexFile, ELIMEX_OLD);
        String newElimexFile = "new_elimex20190121_1112.xlsx";
        new FromXLSX().read(newElimexFile, ELIMEX_NEW);
        ElimexHangtechComp elimexHangtechComp = new ElimexHangtechComp();
        elimexHangtechComp.stockType();
        elimexHangtechComp.compare();
    }

    public static void mapInit(LinkedHashMap<String, LinkedHashMap<String, ArrayList>> map) throws IOException {

        ArrayList<String> columns1 = new ArrayList<>(Arrays.
                asList("sku", "stockStatusName", "alapar"));
        ArrayList<String> columns2 = new ArrayList<>(Arrays.
                asList("Cikkszám", "Nincs készleten állapot", "Alapár"));
        LinkedHashMap<String, ArrayList> export_sheet = new LinkedHashMap<>();
        LinkedHashMap<String, ArrayList> columns_sheet = new LinkedHashMap<>();
        export_sheet.put("Cikkszám", columns2);
        map.put("export", export_sheet);
        columns_sheet.put("sku", columns1);
        columns_sheet.put("Cikkszám", columns2);
        map.put("columns", columns_sheet);
    }
    
    public static void emptyUpload() throws IOException {
        
        LinkedHashMap<String, LinkedHashMap<String, ArrayList>> empty = new LinkedHashMap<>();
        
        mapInit(empty);
        CopyToXLSX copyToUploadXLSX = new CopyToXLSX();
        copyToUploadXLSX.write(empty);
        String time = new Dates().now();
        copyToUploadXLSX.writeout("empty_upload" + time + ".xlsx");
    }
}
