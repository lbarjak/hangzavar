package eu.barjak.hangzavar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Voicekraft implements GlobalVariables {

    Set<String> gyartok = new TreeSet<>(Arrays.asList(
            "AV-LEADER",
            "Elder Audio",
            "FS Audio",
            "Phonic",
            "Roxtone",
            "Seetronic",
            "Voice-Kraft"));

    public void sets() {

        Set<String> intersection = new HashSet<>(VOICEKRAFT_NEW.get("Munka1").keySet());
        intersection.retainAll(HANGZAVAR_MAP.get("export").keySet());
        List<String> intersectionList = new ArrayList<>(intersection);
        Collections.sort(intersectionList);
        intersectionList.forEach(System.out::println);
    }

    public void print() throws IOException {

        VOICEKRAFT_HT.put("van", "Szerdára");
        VOICEKRAFT_HT.put("nincs", "Jelenleg nem elérhető!");

        LinkedHashMap<String, ArrayList> voicekraft_hangzavar = new LinkedHashMap<>();
        HANGZAVAR_MAP.get("export").forEach((key, value) -> {
            gyartok.forEach((gyarto) -> {
                if (value.contains(gyarto)) {
                    voicekraft_hangzavar.put(key, value);
                    Integer alapar = (int) ((Float.parseFloat((String) value.get(50)) + 0.5));
                    value.set(50, alapar.toString());
                }
            });
        });

        System.out.println("Nálunk fent vannak, de már nincsenek a Voice-Kraft listában:");
        System.out.println("Státusz: engedélyezett 1, letiltott 0, kifutott 2");
        voicekraft_hangzavar.forEach((k, v) -> {
            if (!VOICEKRAFT_NEW.get("Munka1").containsKey(k)) {
                //43 --> Státusz (engedélyezett (1) v. letiltott (0) v. kifutott (2)) 
                System.out.println(k + " - "
                        + voicekraft_hangzavar.get(k).get(43).toString().charAt(0));
            }
        });
        System.out.println("---------------------------");

        System.out.println("\nVoice-Kraft változások (csak ami nálunk is van)");
        VOICEKRAFT_NEW.get("Munka1").forEach((key, value) -> {
            if (!value.get(4).equals("Nettó ár")) {
                Integer alapar = (int) ((Float.parseFloat((String) value.get(4)) + 0.5));
                value.set(4, alapar.toString());
            }
            LinkedHashMap<String, ArrayList> v_old = VOICEKRAFT_OLD.get("Munka1");
            v_old.forEach((k, v) -> {
                if (!v.get(4).equals("Nettó ár")) {
                    Integer alapar = (int) ((Float.parseFloat((String) v.get(4)) + 0.5));
                    v.set(4, alapar.toString());
                }
            });
            if (v_old.containsKey(key)
                    && (!value.get(4).equals(v_old.get(key).get(4)) || (!value.get(5).equals(v_old.get(key).get(5))))
                    && voicekraft_hangzavar.containsKey(key)) {
                System.out.println(
                        key + ";"
                        + v_old.get(key).get(4) + ";" + v_old.get(key).get(5) + ";"
                        + value.get(4) + ";" + value.get(5));
                VOICEKRAFT_UPLOAD.get("export").put(key, new ArrayList(Arrays.
                        asList(key, VOICEKRAFT_HT.get(value.get(5)), value.get(4))));
            }
        });

        CopyToXLSX copyToUploadXLSX = new CopyToXLSX();
        copyToUploadXLSX.write(VOICEKRAFT_UPLOAD);
        String time = new Dates().now();
        copyToUploadXLSX.writeout("voicekraft_upload" + time + ".xlsx");
    }
}
