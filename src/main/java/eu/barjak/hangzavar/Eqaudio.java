package eu.barjak.hangzavar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Eqaudio implements GlobalVariables {

    String[] eqaudioFiles = {
        "ecler_arlista_2018_szeptember-szerk.csv",
        "fbt_hangtechnika_arlista_2018_szeptember-szerk.csv",
        "mipro_arlista_2018_szeptember-szerk.csv"};
    String[] eqaudioManufacturers = {
        "Ecler",
        "FBT",
        "Mipro"};
    
    LinkedHashMap<String, String> eq_htech = new LinkedHashMap<>();

    public void txtToXLSX() {

        HANGZAVAR_MAP.get("export").forEach((k, v) -> {
            if (v.get(91) != null
                    && (v.get(91).equals("Ecler")
                    || v.get(91).equals("FBT")
                    || v.get(91).equals("Mipro"))) {
                Float f = Float.parseFloat((String) v.get(50));
                Integer i = (int) (f + 0.5);
                eq_htech.put(k, i.toString());
            }
        });
        for (String fileName : eqaudioFiles) {
            BufferedReader reader = null;
            File file = new File(fileName);
            try {
                reader = new BufferedReader(new FileReader(file));
                String line;
                while ((line = reader.readLine()) != null) {
                    if (eq_htech.containsKey(line.substring(0, line.indexOf(";")))) {
                        System.out.println(line);
                    }
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Eqaudio.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Eqaudio.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        eq_htech.forEach((k, v) -> {
//            System.out.println(k + ";" + v);
//        });
//        for (String key : eq_htech.keySet()) {
//            System.out.println(key + ";" + eq_htech.get(key));
//        }
//        for (HashMap.Entry<String, String> entry : eq_htech.entrySet()) {
//            System.out.println(entry.getKey() + ";" + entry.getValue());
//        }
    }
}