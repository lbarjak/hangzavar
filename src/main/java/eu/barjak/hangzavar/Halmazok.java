package eu.barjak.hangzavar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;

public class Halmazok implements GlobalVariables {

    String aFile = "Jantzen-Dayton-2019-02-11-netsoft-ujakkal_egyutt.csv";
    String bFile = "aktualis_Polink_termeklista_netsoftból.csv";

    public void halmazok() throws IOException, FileNotFoundException, OpenXML4JException {

        //fromXLSX();
        fromCSV(aFile, A_FILE);
        fromCSV(bFile, B_FILE);
        difference();
    }

    public void fromXLSX() throws IOException, FileNotFoundException, OpenXML4JException {

        new FromXLSX().read(aFile, A_FILE);
        new FromXLSX().read(bFile, B_FILE);
    }

    public void fromCSV(String fileName, LinkedHashMap<String, LinkedHashMap<String, ArrayList>> map) throws FileNotFoundException, IOException {

        map.put("sheet1", new LinkedHashMap<>());
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String st;
        while ((st = in.readLine()) != null) {
            ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(st.split(";", 0)));
            if (arrayList.size() > 0) {
                map.get("sheet1").put(arrayList.get(0), arrayList);
            }
        }
    }

    public void difference() throws IOException {

        Set<String> aFileKeys = new HashSet<>(A_FILE.get(A_FILE.keySet().toArray()[0].toString()).keySet());
        Set<String> bFileKeys = new HashSet<>(B_FILE.get(B_FILE.keySet().toArray()[0].toString()).keySet());

        Set<String> difference = new HashSet<>(aFileKeys);
        difference.removeAll(bFileKeys);
        fileWriter("difference", difference);
        Set<String> residual = new HashSet<>(aFileKeys);
        residual.removeAll(difference);
        fileWriter("residual", residual);
        

//        for (String key : difference) {
//            ArrayList row = A_FILE.get(A_FILE.keySet().toArray()[0].toString()).get(key);
//            for (int i = 0; i < row.size(); i++) {
//                System.out.print(row.get(i));
//                if (i < row.size() - 1) {
//                    System.out.print(";");
//                }
//            }
//            System.out.println();
//        }
//        for (String key : residual) {"
//            ArrayList row = A_FILE.get(A_FILE.keySet().toArray()[0].toString()).get(key);
//            for (int i = 0; i < row.size(); i++) {
//                System.out.print(row.get(i));
//                if (i < row.size() - 1) {
//                    System.out.print(";");
//                }
//            }
//            System.out.println();
//        }
    }

    public void fileWriter(String fileName, Set<String> set) throws IOException {
        StringBuilder lf;
        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        String now = formatter.format(today);
        try (FileWriter fw = new FileWriter(fileName + now + ".csv")) {
            for (String key : set) {
                ArrayList row = A_FILE.get(A_FILE.keySet().toArray()[0].toString()).get(key);
                lf = new StringBuilder();
                for (int i = 0; i < row.size(); i++) {
                    lf.append(row.get(i));
                    if (i < row.size() - 1) {
                        lf.append(";");
                    }
                }
                lf.append("\n");
                fw.write(lf.toString());
            }
            fw.close();
        }
    }
}
