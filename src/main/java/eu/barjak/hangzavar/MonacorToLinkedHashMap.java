package eu.barjak.hangzavar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonacorToLinkedHashMap implements GlobalVariables {

    public void html2map() throws MalformedURLException, IOException {

        String match1;
        //<span class="truck">4-7 days</span>
        Pattern pattern1 = Pattern.compile("(?<=<span class=\"truck\">).+(?=</span>)");

        LinkedHashMap<String, ArrayList> monacor = new LinkedHashMap<>();
        HANGZAVAR_MAP.get("export").keySet().forEach((sku) -> {
            if (HANGZAVAR_MAP.get("export").get(sku).get(91) != null
                    && HANGZAVAR_MAP.get("export").get(sku).get(91).equals("Monacor")) {
                monacor.put(sku, HANGZAVAR_MAP.get("export").get(sku));
            }
        });
        //for(String sku : monacor.keySet()) {
        //URL url = new URL("https://www.monacor.hu/product/" + sku.replaceAll("/", "-"));
        URL url = new URL("https://www.monacor.hu/product/" + "ac-082-bl");
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
            Matcher matcher1 = pattern1.matcher(inputLine);
            if (matcher1.find()) {
                match1 = matcher1.group();
                System.out.println(match1);
            }
        }
        //}
    }
}
/*
$.ajax({
                    type: "GET",
                    url: "/product/updateAvailability",
                    data: "id="+art_id+"&qty="+qty,
                    dataType: "json",
                    cache: false,
                    success: function(data){
                        if( data.status == 'ok' ){
                            $('.cms_loader_small').addClass('hidden');
                            $('.availability span').fadeOut('fast').replaceWith(data.html);
                        }
                        else {
                            alert('Error updating availability: ' + data.status);
                        }
                    },
                    error: function(){
                        alert('Error updating availability: ajax request failed');
                    }
                });
 */
