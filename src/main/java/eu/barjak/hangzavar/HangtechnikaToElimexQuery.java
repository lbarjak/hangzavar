package eu.barjak.hangzavar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HangtechnikaToElimexQuery implements GlobalVariables {

    final ArrayList<String> gyartokElimex = new ArrayList<>(Arrays.asList(
            "Adam Hall",
            "BMS",
            "Elimex",
            "Eminence",
            "Gravity",
            "ID-AL",
            "Klotz",
            "König & Meyer",
            "LD Systems",
            "Neutrik",
            "NTI Audio",
            "Palmer",
            "WorldMix"));

    final ArrayList<String> exclude = new ArrayList<>(Arrays.asList(
            "PA-PCAB212**",
            "PA-PCAB212OB**",
            "KL-LY225S, fekete",
            "KM-23110-316-55/CR",
            "NTE-10/3",
            "NTE-1",
            "KL-VD062SH",
            "NA-3FDM",
            "NA-3MDF",
            "NBB-75DFIB",
            "NC-3FP1",
            "NC-5FDL1",
            "NC-5FX",
            "NC-5MDL1",
            "NC-5MX",
            "NDJ-1",
            "NL-8FC",
            "KL-D10KL",
            "NE-8FDP",
            "NE-8FDPB",
            "NE-8FDVYK",
            "NE-8FDYC6",
            "NE-8FDYC6B",
            "NE-8MC"
    ));

    StringBuilder oldal;

    public void stockType() {
        ELIMEX_HT.put("raktáron", "1-2 nap");
        ELIMEX_HT.put("date", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 1-5 munkanap", "1-5 munkanap");
        ELIMEX_HT.put("a megrendeléstől számított 5-10 munkanap", "5-10 munkanap");
        ELIMEX_HT.put("a megrendeléstől számított 10-15 munkanap", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 3-4 hét", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 4-5 hét", "2-4 hét");
        ELIMEX_HT.put("-- átmeneti készlethiány --", "Jelenleg nem elérhető!");
        ELIMEX_HT.put("Keressen a részletekért", "Hívjon!");
        ELIMEX_HT.put("csak előzetes ajánlat alapján", "Hívjon!");
        ELIMEX_HT.put("rendeléskor egyeztetjük", "Hívjon!");
        ELIMEX_HT.put("Kifutó típus, már csak a készlet erejéig szállítható! raktáron", "1-2 nap");
        ELIMEX_HT.put("Kifutó típus, már csak a készlet erejéig szállítható!", "Már nem szállítjuk");
        ELIMEX_HT.put("Megszűnt! Már nem tudjuk szállítani!", "Már nem szállítjuk");
    }

    public void query() throws MalformedURLException, IOException {

        stockType();

        LinkedHashMap<String, ArrayList> Sheet1 = new LinkedHashMap<>();
        ELIMEX_NEW.put("Sheet1", Sheet1);

        ArrayList<String> nincs = new ArrayList<>();

        for (String k : HANGZAVAR_MAP.get("export").keySet()) {
            ArrayList v = HANGZAVAR_MAP.get("export").get(k);

            //3 Terméknév D, 91 Gyártó CN, 96 Nincs készleten állapot CS, 101 Raktárkészlet 1 CX
            if (v.get(91) != null
                    && gyartokElimex.contains((String) v.get(91))
                    && !exclude.contains(k)) {
                ELIMEX_MAP.put(k, v);
            }
        }
        //String k = "CL-NS120";//"NTE-1";//"WM-EA3MRJ";//kifutó raktáron: CL-NS120
        for (String k : ELIMEX_MAP.keySet()) {
            ArrayList v = ELIMEX_MAP.get(k);

            ArrayList<String> page = new ArrayList<>();
            oldal = new StringBuilder();

            URL url = new URL("https://elimex.hu/gyorskereses?q=" + k);
            BufferedReader in;
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                page.add(inputLine);
                oldal.append(inputLine);
            }

            if (page.get(113) != null && page.get(113).contains("Nincs a keresésnek megfelelő találat.")) {
                nincs.add(k + ";" + v.get(101).toString().replace(".0", ""));
                //System.out.println(";;" + k + ";--------------");
            } else {
                //find(k, page);
                search(k, oldal.toString());
            }
        }

        CopyToXLSX copytoxlsx_elimex = new CopyToXLSX();
        copytoxlsx_elimex.write(ELIMEX_NEW);
        String time = new Dates().now();
        copytoxlsx_elimex.writeout("new_elimex" + time + ".xlsx");
        System.out.println("Ezek nálunk vannak de az Elimex-en nincsenek fent + Raktárkészlet 1:");
        for (int i = 0; i < nincs.size(); i++) {
            System.out.println(nincs.get(i));
        }
        //netsoft: Termék név;Nettó eladási egységár;Termék kód
//        ELIMEX_NEW.get(sheet1).forEach((k, v) -> {
//            System.out.println(HANGZAVAR_MAP.get("export").get(k).get(3) + ";" + v.get(2) + ";" + k);
//        });

    }

    public void find(String k, ArrayList page) {

        StringBuilder row = new StringBuilder();
        ArrayList<String> rw;
        String match2 = "";
        String match3 = "";
        String match4 = "";

        Pattern pattern2 = Pattern.compile(
                "raktáron"
                + "|201\\d.\\d{2}.\\d{2}"
                + "|a megrendeléstől számított 1-5 munkanap"
                + "|a megrendeléstől számított 5-10 munkanap"
                + "|a megrendeléstől számított 10-15 munkanap"
                + "|a megrendeléstől számított 3-4 hét"
                + "|a megrendeléstől számított 4-5 hét"
                + "|-- átmeneti készlethiány --"
                + "|Keressen a részletekért"
                + "|csak előzetes ajánlat alapján"
                + "|rendeléskor egyeztetjük"
                + "|Kifutó típus, már csak a készlet erejéig szállítható!"
                + "|Megszűnt! Már nem tudjuk szállítani!");
        Pattern pattern3 = Pattern.compile("raktáron");
        Pattern pattern4 = Pattern.compile("(?<=k\">Az Ön Ára: <span>)\\d{0,3} ?\\d{0,3} ?\\d{0,3}(?= Ft)");

        rw = new ArrayList<>(Arrays.asList("", "", "-"));
        int i = 0;

        Matcher matcher2 = pattern2.matcher((String) page.get(i));
        Matcher matcher3;
        Matcher matcher4 = pattern4.matcher((String) page.get(i));

        while (matcher2.find() == false && i < page.size() - 1) {
            i++;
            matcher2 = pattern2.matcher((String) page.get(i));
        }
        match2 = matcher2.group();
        if ("Kifutó típus, már csak a készlet erejéig szállítható!".equals(match2)) {
            matcher3 = pattern3.matcher((String) page.get(i));
            if (matcher3.find()) {
                match3 = " " + matcher3.group();
            }
        }
        match2 += match3;
        match2 = ELIMEX_HT.get(match2);

        while (matcher4.find() == false && i < page.size() - 1) {
            i++;
            matcher4 = pattern4.matcher((String) page.get(i));
        }
        match4 = matcher4.group();

        //System.out.println(k + ";" + match2 + ";" + match4.replace(" ", ""));
        //System.out.println(";;" + match4.replace(" ", "") + ";" + match2 + ";" + k);
        row.append(match2);

        rw.set(0, k);
        rw.set(1, row.toString());
        rw.set(2, match4.replace(" ", ""));
        row.setLength(0);
        //ELIMEX_NEW.get("Sheet1").put(k, rw);
        match2 = "";
        match3 = "";
        match4 = "";

        //System.out.println(k + " " + ELIMEX_NEW.get("Sheet1").get(k));
    }

    public void search(String k, String page) {

        Document doc = Jsoup.parse(oldal.toString());
        String text = doc.body().text();
        ArrayList<String> rw = new ArrayList<>(Arrays.asList(k, "", "-"));
        String match_a = "";
        String match_b = "";
        String match_c = "";
        String match_cn = "";
        Pattern pattern_a = Pattern.compile(
                "raktáron"
                + "|201\\d.\\d{2}.\\d{2}"
                + "|a megrendeléstől számított 1-5 munkanap"
                + "|a megrendeléstől számított 5-10 munkanap"
                + "|a megrendeléstől számított 10-15 munkanap"
                + "|a megrendeléstől számított 3-4 hét"
                + "|a megrendeléstől számított 4-5 hét"
                + "|-- átmeneti készlethiány --"
                + "|Keressen a részletekért"
                + "|csak előzetes ajánlat alapján"
                + "|rendeléskor egyeztetjük"
                + "|Kifutó típus, már csak a készlet erejéig szállítható!"
                + "|Megszűnt! Már nem tudjuk szállítani!");
        Pattern pattern_b = Pattern.compile("raktáron");
        Pattern pattern_c = Pattern.compile("(?<=Az Ön Ára: )\\d{0,3} ?\\d{0,3} ?\\d{0,3}(?= Ft)");
        
        if (text.contains(k)) {
            Matcher matcher_a = pattern_a.matcher(text);
            Matcher matcher_c = pattern_c.matcher(text);
            if (matcher_a.find()) {
                match_a = matcher_a.group();
            }
            if ("Kifutó típus, már csak a készlet erejéig szállítható!".equals(match_a)) {
                Matcher matcher_b = pattern_b.matcher(text);
                if (matcher_b.find()) {
                    match_b = " " + matcher_b.group();
                }
            }
            if (matcher_c.find()) {
                match_c = matcher_c.group();
                Integer match_ci = (int) ((Float.parseFloat(match_c) + 0.5) / 1.27);
                match_cn = match_ci.toString();
            }
            match_a += match_b;
            match_a = ELIMEX_HT.get(match_a);

            //System.out.println(k + ";" + match_a + ";" + match_c.replace(" ", ""));

            rw.set(1, match_a);
            //rw.set(2, match_c.replace(" ", ""));
            rw.set(2, match_cn + "");
            ELIMEX_NEW.get("Sheet1").put(k, rw);
            //System.out.println(HANGZAVAR_MAP.get("export").get(k).get(3) + ";" + ELIMEX_NEW.get("Sheet1").get(k).get(2) + ";" + k);
            System.out.println(ELIMEX_NEW.get("Sheet1").get(k).get(2) + ";" + k);
            match_a = "";
            match_b = "";
            match_c = "";
        }
    }
}
