package eu.barjak.hangzavar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FromXLSX {

    String sheetName;

    public void read(String xlsxName, LinkedHashMap<String, LinkedHashMap<String, ArrayList>> output)
            throws FileNotFoundException, IOException, InvalidFormatException, OpenXML4JException {

        OPCPackage fis = OPCPackage.open(new FileInputStream(xlsxName));

        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);

        XSSFReader read = new XSSFReader(fis);
        Iterator<InputStream> sheetsIterator = read.getSheetsData();
        XSSFReader.SheetIterator sheet = (XSSFReader.SheetIterator) sheetsIterator;

        while (sheet.hasNext()) {
            sheet.next();
            sheetName = sheet.getSheetName();
            System.out.println(xlsxName + " / " + sheetName);
            output.put(sheetName, new LinkedHashMap<>());
            XSSFSheet mySheet = myWorkBook.getSheet(sheetName);

            int numberOfColumns = mySheet.getRow(0).getPhysicalNumberOfCells();
            System.out.println("numberOfRows: " + mySheet.getPhysicalNumberOfRows() + "\n");

            for (Row row : mySheet) {
                ArrayList<String> rowOfArlistaArrayList = new ArrayList<>(Collections.nCopies(numberOfColumns, null));

                for (int c = 0; c < numberOfColumns; c++) {
                    Cell cell = row.getCell(c);

                    if (!(cell == null)) {
                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_STRING:
                                rowOfArlistaArrayList.set(c, cell.getStringCellValue().trim());
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                rowOfArlistaArrayList.set(c, String.valueOf(cell.getNumericCellValue()));
                                break;
                            case Cell.CELL_TYPE_BOOLEAN:
                                rowOfArlistaArrayList.set(c, String.valueOf(cell.getBooleanCellValue()));
                                break;
                            default:
                        }
                    }
                }
                String key = rowOfArlistaArrayList.get(0).replace(".0", "");
                output.get(sheetName).put(key, new ArrayList<>(rowOfArlistaArrayList));
            }
        }
    }
}
