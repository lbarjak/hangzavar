/*
Végignézi az ElimexToMap oldalát, a html-ből kikeresi az adatokat és az ELIMEX_NEW-ba írja.
Formátuma key cikkszám; value [cikkszám, elérhetőség, bruttó ár]:
...
RO-SB324K30;[RO-SB324K30, raktáron, 146000]
RO-SB328K30D;[RO-SB328K30D, raktáron, 184000]
EX-AU10000;[EX-AU10000, raktáron, 10000]
WM-EL4MM;[WM-EL4MM, raktáron, 660]
WM-EAC3FCA;[WM-EAC3FCA, raktáron, 980]
WM-EAC3FPX;[WM-EAC3FPX, raktáron, 980]
WM-EAC3MPA;[WM-EAC3MPA, raktáron, 620]
WM-EAC3MPX;[WM-EAC3MPX, raktáron, 980]
RO-UC1001;[RO-UC1001, raktáron, 8640]
...
 */
package eu.barjak.hangzavar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;

public class ElimexToLinkedHashMap implements GlobalVariables {

    ArrayList<String> rw;

    public void html2map() throws IOException, FileNotFoundException, OpenXML4JException {

        LinkedHashMap<String, ArrayList> Sheet1 = new LinkedHashMap<>();
        ELIMEX_NEW.put("Sheet1", Sheet1);
        StringBuilder row = new StringBuilder();
        int prod;
        int page;
        String match0;//********************
        String match1;
        String match2 = "";
        String match3 = "";
        String match4;
        Pattern pattern0 = Pattern.compile("selectvariant");//********************
        Pattern pattern1 = Pattern.compile("(?<=>)[A-Z\\d-+]+(?=</a></span><br)");//113. sor
        Pattern pattern2 = Pattern.compile(
                "raktáron"
                + "|201\\d.\\d{2}.\\d{2}"
                + "|a megrendeléstől számított 1-5 munkanap"
                + "|a megrendeléstől számított 5-10 munkanap"
                + "|a megrendeléstől számított 10-15 munkanap"
                + "|a megrendeléstől számított 3-4 hét"
                + "|a megrendeléstől számított 4-5 hét"
                + "|-- átmeneti készlethiány --"
                + "|Keressen a részletekért"
                + "|csak előzetes ajánlat alapján"
                + "|rendeléskor egyeztetjük"
                + "|Kifutó típus, már csak a készlet erejéig szállítható!"
                + "|Megszűnt! Már nem tudjuk szállítani!");
        Pattern pattern3 = Pattern.compile("raktáron");
        Pattern pattern4 = Pattern.compile("(?<=k\">Az Ön Ára: <span>)\\d{0,3} ?\\d{0,3} ?\\d{0,3}(?= Ft)");

        for (prod = 75; prod <= 1270; prod++) {//75-1270, 1188, 1265
            for (page = 1; page < 12; page++) {
                //for (prod = 1261; prod <= 1261; prod++) {//75, 1188, 1265
                //for (page = 1; page < 2; page++) {
                //int il = 0;
                //https://elimex.hu/gyartokategoriak/1261?list&page=1
                try {
                    URL url = new URL("https://elimex.hu/gyartokategoriak/" + prod + "?list&page=" + page);
                    BufferedReader in;
                    try {
                        in = new BufferedReader(new InputStreamReader(url.openStream()));
                        String inputLine;
                        while ((inputLine = in.readLine()) != null) {
                            Matcher matcher0 = pattern0.matcher(inputLine);
                            Matcher matcher1 = pattern1.matcher(inputLine);
                            Matcher matcher2 = pattern2.matcher(inputLine);
                            Matcher matcher3 = pattern3.matcher(inputLine);
                            Matcher matcher4 = pattern4.matcher(inputLine);
                            if (matcher4.find()) {
                                match4 = matcher4.group().replace(" ", "");
                                Integer match4i = (int) ((Float.parseFloat(match4) + 0.5) / 1.27);
                                rw.set(2, match4i.toString());
                            }
                            if (matcher1.find()) {
                                rw = new ArrayList<>(Arrays.asList("", "", "-"));
                                match1 = matcher1.group();
                                if (matcher2.find()) {
                                    match2 = matcher2.group();
                                    if ("Kifutó típus, már csak a készlet erejéig szállítható!".equals(matcher2.group())) {
                                        if (matcher3.find()) {
                                            match3 = matcher3.group();
                                        }
                                    }
                                }
                                row.append(match2);
                                if (!"".equals(match3)) {
                                    row.append(" ").append(match3);
                                    match3 = "";
                                }
                                rw.set(0, match1);
                                rw.set(1, row.toString());
                                row.setLength(0);
                                ELIMEX_NEW.get("Sheet1").put(match1, rw);
//                                if (matcher0.find() && HANGZAVAR_MAP.get("export").containsKey(match1)) {
//                                    match0 = matcher0.group();
//                                    System.out.print(match0 + " prod: " + prod + " page: " + page);
//                                    System.out.println(" " + ELIMEX_NEW.get("Sheet1").get(match1));
//                                }
                                //System.out.println(prod + " " + page + " " + ELIMEX_NEW.get("Sheet1").get(match1));
                                match2 = "";
                            }
                        }
                    } catch (IOException e) {
                        page = 12;
                        System.out.println((100 * prod / 1265) - 5 + "%");
                    }
                } catch (MalformedURLException ex) {
                    Logger.getLogger(ElimexToLinkedHashMap.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        ELIMEX_NEW.keySet().forEach((sheetName) -> {
            System.out.println("sheetName: " + sheetName);
        });
        CopyToXLSX copytoxlsx_elimex = new CopyToXLSX();
        copytoxlsx_elimex.write(ELIMEX_NEW);
        String time = new Dates().now();
        copytoxlsx_elimex.writeout("new_elimex" + time + ".xlsx");

        //FromXLSX fromxlsx = new FromXLSX();
        //fromxlsx.read("old_elimex20180908_2236.xlsx", ELIMEX_OLD);
    }

    public void print() {//csak tesztre
        ELIMEX_NEW.get("Sheet1").forEach((k, v) -> {
            System.out.println(k + ";" + v);
        });
//        System.out.println("--------------------------------------------");
//        ELIMEX_OLD.get("Sheet1").forEach((k, v) -> {
//            System.out.println(k + ";" + v);
//        });
    }
}
