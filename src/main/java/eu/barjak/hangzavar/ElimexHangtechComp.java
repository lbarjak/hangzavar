/*
A Hangtechnika hangzavar áruházban használt elérhetőségekre cseréli
az Elimex-nél használtakat.
Az előző heti állapottal végez összehasonlítást.
A teljes HANGZAVAR_MAP-ban keres, nem használja a gyartokElimex-et
 */
package eu.barjak.hangzavar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ElimexHangtechComp implements GlobalVariables {

    public void stockType() {
        ELIMEX_HT.put("raktáron", "1-2 nap");
        ELIMEX_HT.put("date", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 1-5 munkanap", "1-5 munkanap");
        ELIMEX_HT.put("a megrendeléstől számított 5-10 munkanap", "5-10 munkanap");
        ELIMEX_HT.put("a megrendeléstől számított 10-15 munkanap", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 3-4 hét", "2-4 hét");
        ELIMEX_HT.put("a megrendeléstől számított 4-5 hét", "2-4 hét");
        ELIMEX_HT.put("-- átmeneti készlethiány --", "Jelenleg nem elérhető!");
        ELIMEX_HT.put("Keressen a részletekért", "Hívjon!");
        ELIMEX_HT.put("csak előzetes ajánlat alapján", "Hívjon!");
        ELIMEX_HT.put("rendeléskor egyeztetjük", "Hívjon!");
        ELIMEX_HT.put("Kifutó típus, már csak a készlet erejéig szállítható! raktáron", "1-2 nap");
        ELIMEX_HT.put("Kifutó típus, már csak a készlet erejéig szállítható!", "Már nem szállítjuk");
        ELIMEX_HT.put("Megszűnt! Már nem tudjuk szállítani!", "Már nem szállítjuk");
    }

    public void compare() throws IOException {

        stockType();

        //1234.0 --> 1234 korrekció
//        ELIMEX_OLD.get("Sheet1").forEach((k, v) -> {
//            String s = (String) v.get(2);
//            ELIMEX_OLD.get("Sheet1").get(k).set(2, s.replace(".0", ""));
//        });
        //kerekítés egyesekre
//        ELIMEX_OLD.get("Sheet1").forEach((k, v) -> {
//            Float f = Float.parseFloat((String) v.get(2));
//            Integer i = (int) (f + 0.5);
//            ELIMEX_OLD.get("Sheet1").get(k).set(2, i.toString());
//        });

        System.out.println("ELIMEX");
        System.out.println("------");
        System.out.println();
        System.out.println("Újak");
        System.out.println("----");

        Set plus = new LinkedHashMap(ELIMEX_NEW.get("Sheet1")).keySet();
        plus.removeAll(ELIMEX_OLD.get("Sheet1").keySet());
        plus.forEach((k) -> {
        ArrayList p = (ArrayList) ELIMEX_NEW.get("Sheet1").get(k);
            System.out.print(p.get(0) + ";");
            System.out.print(p.get(1) + ";");
            System.out.println(p.get(2));
        });

        System.out.println();
        System.out.println("Megszűntek - csak ami nálunk is van");
        System.out.println("-----------------------------------");
        Set minus = new LinkedHashMap(ELIMEX_OLD.get("Sheet1")).keySet();
        minus.removeAll(ELIMEX_NEW.get("Sheet1").keySet());
        minus.forEach((k) -> {
            if (HANGZAVAR_MAP.get("export").containsKey((String) k)) {
                ArrayList m = (ArrayList) ELIMEX_OLD.get("Sheet1").get(k);
                System.out.print(m.get(0) + ";");
                System.out.print(m.get(1) + ";");
                System.out.println(m.get(2));
            }
        });

        System.out.println();
        System.out.println("Változott az elérhetőségük - csak ami nálunk is van:");
        System.out.println("----------------------------------------------------");
        String rx = "201\\d.\\d{2}.\\d{2}";
        ELIMEX_NEW.get("Sheet1").keySet().forEach((key) -> {
            if (ELIMEX_OLD.get("Sheet1").containsKey(key)
                    && !ELIMEX_NEW.get("Sheet1").get(key).get(1).
                            equals(ELIMEX_OLD.get("Sheet1").get(key).get(1))
                    && HANGZAVAR_MAP.get("export").containsKey(key)) {
                System.out.println(
                        key + ";"
                        + ELIMEX_HT.get(ELIMEX_OLD.get("Sheet1").get(key).get(1).
                                toString().replaceAll(rx, "date")) + ";"
                        + ELIMEX_HT.get(ELIMEX_NEW.get("Sheet1").get(key).get(1).
                                toString().replaceAll(rx, "date")));
            }
        });

        System.out.println();
        System.out.println("Változott az áruk - csak ami nálunk is van:");
        System.out.println("(* az elérhetőség is változott)");
        System.out.println("(0 árú termék nem kerül bele az xlsx-be)");
        System.out.println("-------------------------------------------");
        ELIMEX_NEW.get("Sheet1").keySet().forEach((key) -> {
            if (ELIMEX_OLD.get("Sheet1").containsKey(key)
                    && !ELIMEX_NEW.get("Sheet1").get(key).get(2).
                            equals(ELIMEX_OLD.get("Sheet1").get(key).get(2))
                    && HANGZAVAR_MAP.get("export").containsKey(key)) {
                System.out.print(
                        key + ";"
                        + ELIMEX_OLD.get("Sheet1").get(key).get(2)
                        + ";"
                        + ELIMEX_NEW.get("Sheet1").get(key).get(2)
                );
                if (ELIMEX_OLD.get("Sheet1").containsKey(key)
                        && !ELIMEX_NEW.get("Sheet1").get(key).get(1).
                                equals(ELIMEX_OLD.get("Sheet1").get(key).get(1))
                        && HANGZAVAR_MAP.get("export").containsKey(key)) {
                    System.out.println(" *");
                } else {
                    System.out.println();
                }
            }
        });

        ELIMEX_NEW.get("Sheet1").keySet().forEach((key) -> {
            if (ELIMEX_OLD.get("Sheet1").containsKey(key)
                    && HANGZAVAR_MAP.get("export").containsKey(key)
                    && (!ELIMEX_NEW.get("Sheet1").get(key).get(1).
                            equals(ELIMEX_OLD.get("Sheet1").get(key).get(1))
                    || !ELIMEX_NEW.get("Sheet1").get(key).get(2).
                            equals(ELIMEX_OLD.get("Sheet1").get(key).get(2)))) {
                String new1 = ELIMEX_HT.get(ELIMEX_NEW.get("Sheet1").get(key).get(1).toString());
                String new2 = ELIMEX_NEW.get("Sheet1").get(key).get(2).toString();
                if (!new2.equals("0")) {//nulla árú nem kerül az xlsx-be
                    ELIMEX_UPLOAD.get("export").put(key, new ArrayList(Arrays.
                            asList(key, new1, new2)));
                }
            }
        });

        System.out.println();

        System.out.println("Upload:");
        System.out.println("-------");
        ELIMEX_UPLOAD.get("export").forEach((k, v) -> {
            System.out.println(k + ";" + v);
        });
        System.out.println("---------------------------------------------");
        ELIMEX_UPLOAD.get("columns").forEach((k, v) -> {
            System.out.println(k + ";" + v);
        });
        CopyToXLSX copyToUploadXLSX = new CopyToXLSX();
        copyToUploadXLSX.write(ELIMEX_UPLOAD);
        String time = new Dates().now();
        copyToUploadXLSX.writeout("elimex_upload" + time + ".xlsx");
        
        ////////////////////////////////////////////////////////////////
        System.out.println("\nElimex-ből csak amik a Hangtechnika áruházban fent vannak:");
        ELIMEX_NEW.get("Sheet1").keySet().forEach((key) -> {
            if(HANGZAVAR_MAP.get("export").containsKey(key)){
                System.out.println(key + "; " + ELIMEX_NEW.get("Sheet1").get(key).get(2));
            }
        });
    }
}
